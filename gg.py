# -*- coding: utf-8 -*-
import argparse
import inspect
import os
import signal
import sys

#[START] Local import
from include.checkers import checkers
from include.exceptions import exceptions as ex
from include.exceptions import handler_exceptions
from include.modules import utils
from include.modules.setup import setup
#[END] Local import

CWD = os.getcwd()
FILENAME = inspect.getframeinfo(inspect.currentframe()).filename
PATHBASE = os.path.dirname(os.path.abspath(FILENAME))

def clean_up(signum, frame):
	print "Proceso interrmpido."
	print "Se eliminaran archivos .pyc"
	try:
		utils.del_files_in_tree(rootdir=CWD, suffix='.pyc')	
		utils.del_files_in_tree(rootdir=PATHBASE, suffix='.pyc')
	except Exception as e:
		print e
		sys.exit(2)
	else:
		print "Archivos .pyc eliminados"
	sys.exit(0)

def main():
	parser = argparse.ArgumentParser()
	subparser = parser.add_subparsers(dest='command')
	
	for cmd in ['create','c']:
		spp = subparser.add_parser(cmd,
		help="Crea algun componente, ya sea el proyecto o alguna ruta con su handler.")
		spp.add_argument('component', type=str)
		spp.add_argument('name', type=str)

	for cmd in ['serve', 's']:
		spp = subparser.add_parser(cmd,
		help="Ejecuta dev_appserver despues de eliminar todos los archivos .pyc del arbol del proyecto.")

	for cmd in ['burn', 'b']:
		spp = subparser.add_parser(cmd,
		help="Elimina toda el arbol del proyecto. Solo si su configuracion lo permite")
	
	for cmd in ['deploy', 'dp']:
		spp = subparser.add_parser(cmd,
		help="Permite la automatizacion del despliegue del proyecto en GCP")
		
	setup_info = parser.parse_args()

	checker = checkers.Checker(filetype="setup_info")

	# try:
	checker.load_info(setup_info)
	setup(setup_info, CWD, PATHBASE)
	# except Exception as e:
		# handler_exceptions.handler_exception(e)

if __name__ == '__main__':
	signal.signal(signal.SIGINT, clean_up)

	main()