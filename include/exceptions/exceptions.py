from include.checkers import accepted_strings

class InvalidComponentError(Exception):
    valid_component = '\n*) '.join('{}'.format(k) for k in accepted_strings.COMPONENTS)
    message = '''
Componente invalido:

Los componentes validos son:

*) {componentes}'''.format(componentes=valid_component)

class InvalidCommandError(Exception):
    valid_commands = '\n*) '.join('{}'.format(k) for k in accepted_strings.COMMANDS)
    message = '''
Comando invalido:

Los comandos validos son:

*) {comandos}'''.format(comandos=valid_commands)

class InvalidComponentCreationError(Exception):
    pass

class NotRouterClassError(Exception):
    pass

class NotRoutesListError(Exception):
    pass

class ConfigFileError(Exception):
    def __init__(self):
        print '\n\n\tRevisa si tienes el archivo de configuracion\n\n'

class DeploymentConfigError(Exception):
    def __init__(self):
        print '\nRevisa si on_gae esta en True\n'

class ComponenteCreationError(Exception):
    pass
