import inspect

from include.exceptions import exceptions as ex

def handler_exception(e):
	error = e.__class__.__name__
	error_list = [m[0] for m in inspect.getmembers(ex, inspect.isclass) if m[1].__module__ == 'handlers.exceptions.exceptions']
	if error in error_list:
		print e.message
	else:
		print e