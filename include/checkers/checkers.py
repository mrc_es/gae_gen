
import re
import yaml

from include.checkers import accepted_strings as a_s
from include.exceptions import exceptions as ex

class Checker:

    def __init__(self, filetype):   

        self.filetype = filetype

    def validate_app_name(self, app_name):
        pass

    def load_info(self, info):  return getattr(self,'validate_' + self.filetype)(info)

    def validate_setup_info(self, setup_info):
        
        if setup_info.command not in a_s.COMMANDS:
            raise ex.InvalidCommandError

        if hasattr(setup_info, 'component'):
            if setup_info.component not in a_s.COMPONENTS:
                raise ex.InvalidComponentError

    def validate_route_info(self, body):
        
        classes = filter(
            lambda x: x.__class__.__name__ == 'ClassDef', body.body )
        
        if not classes:
            raise ex.NotRouterClassError

        try:
            router_p = filter(lambda x: x.name == 'Router', classes)[0]
        except IndexError:
            raise ex.NotRouterClassError            

        listas = filter(
            lambda x: x.__class__.__name__ == 'Assign',router_p.body)

        routes_list = listas[0].value.elts

        listas = filter(
            lambda x: x.value.elts[0].func.value.id == 'webapp2', listas)

        if not listas:
            raise ex.NotRoutesListError

        return router_p

    def validate_config_yaml(self, config_yaml):
        config_info = yaml.load(config_yaml)
        missing_keys = set(a_s.REQUIRED_YAML_CONFIG_FIELDS) - set(config_info.keys())

        if missing_keys:
            msg = 'Faltan los siguientes argumentos en el archivo de configuracion:\n{}'.format(
                    '\n'.join( map('"{}"'.format, missing_keys) )
                    )
            ex.ConfigFileError.message = msg
            raise ex.ConfigFileError

        return config_info
    
    def validate_deployment_config(self, deployment_info):
        if not deployment_info['on_gae']:
            raise ex.DeploymentConfigError

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass