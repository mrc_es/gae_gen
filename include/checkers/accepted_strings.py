
COMMANDS = [
	'create',
	'add',
	'delete',
	'burn',
	'serve',
	'deploy',
	'c',
	'a',
	'd',
	'b',
	's',
	'dp'
]

COMPONENTS = [
	'project',
	'route',
	'storage',
	's',
	'p',
	'r'
]

INIT_TEMPLATES = [
	'.gitignore',
	'__init__.py',
	'app.yaml',
	'appengine_config.py',
	'README.md',
	'requirements.txt',
	'.gg_config.yaml'
]

TEMPLATES = ['handler_template.py']

REQUIRED_YAML_CONFIG_FIELDS = ['gg_project_id',
								'on_gae' ]