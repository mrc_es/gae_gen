import os
import shutil
from shutil import ignore_patterns
import uuid

from include.checkers import accepted_strings as a_s

def create_project(setup_info, cwd, pathbase):

    print('\nCreando proyecto {} ...\n'.format(setup_info.name))
    
    #[START] Path variables
    cwd = os.path.join(cwd, setup_info.name)
    SRC_PREBUILD = os.path.join(pathbase, 'prebuild')
    SRC_TEMPLATES = os.path.join(pathbase, 'templates')
    #[END] Path variables
    
    #while True:
        
        #if os.path.exists(cwd):
    #        break
        
        #shutil.os.mkdir(cwd)

    #[START] Copiando templates iniciales (INIT_TEMPLATES) a cwd
    print('''Copiando templates iniciales...''')
    shutil.copytree(SRC_PREBUILD, cwd, ignore=ignore_patterns('datastore*','storage') )

    for file_name in os.listdir(SRC_TEMPLATES):
        full_file_name = os.path.join(SRC_TEMPLATES, file_name)
        if os.path.isfile(full_file_name) and file_name in a_s.INIT_TEMPLATES:
            shutil.copy(full_file_name, cwd)

    print('''Terminando de copiar templates iniciales''')
    #[END] Copiando templates iniciales (INIT_TEMPLATES) a cwd

    #[START] Editar archivo app_yaml
    print('Editando app.yaml...')
    with open(os.path.join(cwd, 'app.yaml'), 'r') as config_yaml:
        new_config_yaml = config_yaml.read().format(service_name=setup_info.name)

    with open(os.path.join(cwd, 'app.yaml'), 'w') as config_yaml:
        config_yaml.write(new_config_yaml)
    print('Terminando de editar app.yaml')
    #[END] Editar archivo app_yaml

    #[START] Editar archivo de configuracion del proyecto
    print('Editando archivo de configuracion del proyecto...')
    gg_project_id = str(uuid.uuid4())
    with open(os.path.join(cwd, '.gg_config.yaml'), 'r') as gg_config:
        new_gg_config = gg_config.read().format(project_id=gg_project_id, service_name=setup_info.name)
    
    with open(os.path.join(cwd, '.gg_config.yaml'), 'w') as gg_config:
        gg_config.write(new_gg_config)
    print('Terminando de editar archivo de configuracion del proyecto')
    #[END] Editar archivo de configuracion del proyecto

    print('\nFinalizo la creacion del proyecto {} \n'.format(setup_info.name))