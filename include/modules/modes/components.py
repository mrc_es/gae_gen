from include.modules import components

class CreateComponent:

    component_dict = {
        "route":['route', 'r'],
        "datastore":['datastore', 'ds'],
    }

    def __init__(self, setup_info, cwd, pathbase):
        self.component_alias = setup_info
        self.component_args = [cwd, pathbase, setup_info]

    def get_real_component_name(self, component_alias):
        for real_name, list_alias in self.component_dict.iteritems():
            for alias in list_alias:
                if alias == component_alias: 
                    return real_name
    
    def create(self):
        if self.component_alias.component in ['storage','s']:
            component_name = self.get_real_component_name(
                                    self.component_alias.name.lower())
        else:
            component_name = self.get_real_component_name(
                                    self.component_alias.component.lower())

        cc = components.prepare(component_name, *self.component_args)
        cc.create()