
import subprocess

from include.modules import utils

class LocalTesting:

    def __init__(self, cwd, pathbase):
        self.cwd = cwd
        self.pathbase = pathbase

    def serve(self):
        utils.del_files_in_tree(rootdir=self.pathbase, suffix='.pyc')
        utils.del_files_in_tree(rootdir=self.cwd, suffix='.pyc')
        subprocess.check_output('dev_appserver.py .', shell=True)