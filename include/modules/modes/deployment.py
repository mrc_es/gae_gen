
import subprocess

from ruamel import yaml

from include.checkers.checkers import Checker
import include.modules.utils

deployment_string = 'gcloud app deploy {version} {project} {promote} --format=yaml -q'

def construct_deployment_string(config_info):
    global deployment_string    
    
    ci = config_info['gae_deploy_info']

    if ci['project']:
        deployment_string = deployment_string.replace('{project}',
                                                    '--project=' + ci['project'])
    else:
        deployment_string = deployment_string.replace('{project}', '')

    if ci['promote']:
        deployment_string = deployment_string.replace('{promote}', '--promote')
    else:
        deployment_string = deployment_string.replace('{promote}', '--no-promote')

    if ci['version']:
        deployment_string = deployment_string.replace('{version}', '-v ' + ci['version'])

    return yaml.dump(ci, Dumper=yaml.RoundTripDumper), deployment_string

def deploy(cwd, config_info):

    with Checker(filetype='deployment_config') as checker:
        checker.load_info(config_info)

    yaml_info, deployment_string = construct_deployment_string(config_info)

    print '\nConfiguracion de archivo de despliegue:\n\n{}'.format(yaml_info)
    
    print 'Cadena de despliegue: \n\n\t{}\n'.format(deployment_string)

    while True:
        respuesta = raw_input("Desea iniciar el despliegue de sus servicios? (s/n): ")

        if respuesta in ['s', 'S', 'n', 'y', 'Y']:
            break
        print "Escriba 's' o 'n'"

    if respuesta in ['s','y','Y', 'S']: 
        subprocess.check_output(deployment_string, shell=True)
    else: 
        print "Eligio: '{}', :)".format(respuesta)