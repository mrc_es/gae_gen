import os
import shutil
import yaml

from include.exceptions import exceptions as ex

def check_burn_flag(cwd, config_info):
	if config_info['burn']:
		o = ''
		while o not in ['s', 'n', 'y']:
			o = raw_input("Desea borrar arbol de proyecto? (s/n): ")
		
		if o in ['s','y']: 
			return True
		else: 
			print("Operacion cancelada")
			return False
	else:
		message = "El archivo de configuracion tiene bloqueada esta opcion\nPara habilitarla, cambie el parametro a True"
		ex.ConfigFileError.message = message
		raise ex.ConfigFileError

def burn(cwd, config_info):
	if check_burn_flag(cwd, config_info):
		shutil.rmtree(cwd, ignore_errors=True)