import os
import string
import ast

import astunparse

def recursive_glob(rootdir='.', suffix=''):
    target_files = [os.path.join(looproot, filename)
                    for looproot, _, filenames in os.walk(rootdir)
                    for filename in filenames if filename.endswith(suffix)]
    return target_files

def del_files_in_tree(**kw):
    for file_to_erase in recursive_glob(**kw):
        os.remove(file_to_erase)


class DefaultFormatter(string.Formatter):
    """DefaultFormatter

    Anade una cadena default a una cadena de tipo
    'foo {bar} foo {baz}'
    En caso de que bar o baz no esten definidas, 
    les anade una cadena default.

    Forma de uso 1:

    cadena = 'foo {bar} foo {baz}'

    with DefaultFormatter() as df:
       print(df.format(cadena, bar='baz'))
    
    resultado: 'foo baz foo '
    ya que por default '' (cadena vacia) esta definido.

    Forma de uso 2:

    cadena = 'foo {bar} foo {baz}'

    df = DefaultFormatter(default="default_string"):
    print(df.format(cadena, bar='baz'))

    resultado: 'foo baz foo default_string'

    Extends:
    string.Formatter
    """

    def __init__(self, default=''):
        self.default=default

    def get_value(self, key, args, kwds):
        if isinstance(key, str):
            return kwds.get(key, self.default)
        else:
            return string.Formatter.get_value(key, args, kwds)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

def get_index_for_classname(name, l): return [ i for i, j in enumerate(l) if l[i].__class__.__name__ == name ]

def create_route(routes_file, route_name, module_handler, class_handler, handler_method):
    route_template = '''webapp2.Route(r'/{route}<:/?>', handler={module_handler}.{class_handler}, handler_method='{handler_method}')'''

    with open(routes_file, 'r') as f:
        codigo = f.read()

    full_body = ast.parse(codigo)

    classes = filter(
        lambda x: x.__class__.__name__ == 'ClassDef', full_body.body )
    
    router = filter(lambda x: x.name == 'Router', classes)[0]

    lista = filter(
        lambda x: x.__class__.__name__ == 'Assign',router.body)

    #lista parseada de rutas
    routes_list = lista[0].value.elts

    #lista de rutas en caracteres
    unparsed_list = [
            astunparse.unparse(parsed_routes) 
            for parsed_routes in routes_list ]

    route_template_info = {'route': route_name,
                            'module_handler': module_handler,
                            'class_handler': class_handler,
                            'handler_method': handler_method }

    unparsed_list.append(route_template.format(**route_template_info))

    parsed_list = [
        ast.parse(p)
        for p in unparsed_list
    ]
    
    #Buscando la lista de rutas en cuerpo del codigo parseado para
    #luego anadirle la nueva ruta
    i = get_index_for_classname("ClassDef", full_body.body)[0]
    j = get_index_for_classname("Assign", full_body.body[i].body)[0]
    
    full_body.body[i].body[j].value.elts = parsed_list

    #Despues de modificar la ruta, se "desparsea" y eso se ingresa
    #al codigo a modificar
    new_code = astunparse.unparse(full_body)

    with open(routes_file, 'w') as f:
        codigo = f.write(new_code)