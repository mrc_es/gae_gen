import os

from include.checkers.checkers import Checker
from include.exceptions import exceptions as ex
#[START] setup modes modules
from include.modules.modes.burn import burn
from include.modules.modes.projects_operations import create_project
from include.modules.modes.deployment import deploy
from include.modules.modes.components import CreateComponent
from include.modules.modes.serve import LocalTesting
#[END] setup modes modules

def validate_config_file(cwd):

    gg_config_yaml = os.path.join(cwd, '.gg_config.yaml')
    
    try:
        with Checker(filetype='config_yaml') as checker, open(gg_config_yaml,'r') as cy:
           return checker.load_info(cy.read())
    except IOError:
        msg = 'El archivo de configuracion {} no se encuentra en la carpeta actual '.format('.gg_config.yaml')
        ex.ConfigFileError.message = msg
        raise ex.ConfigFileError

def setup(setup_info, cwd, pathbase):
    
    if setup_info.command in ['create', 'c'] and setup_info.component in ['project', 'p']:
        create_project(setup_info, cwd, pathbase)
        exit(0)
    
    '''La validacion del la informacion va aqui porque
    en caso de ser la primera vez que se crea el proyecto,
    no hay archivo de configuracion y no habra motivo por el cual
    parar la creacion.
    '''
    config_info = validate_config_file(cwd)

    if setup_info.command in ['create', 'c'] and setup_info.component:
        c = CreateComponent(setup_info, cwd, pathbase)
        c.create()
    elif setup_info.command in ['serve', 's']:
        l_server = LocalTesting(cwd, pathbase)
        l_server.serve()
    elif setup_info.command in ['burn', 'b']:
        burn(cwd, config_info)
    elif setup_info.command in ['deploy', 'dp']:
        deploy(cwd, config_info)