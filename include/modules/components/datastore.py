import inspect
import os
import shutil

from include.exceptions import exceptions
from include.checkers import accepted_strings as a_s
from include.checkers.checkers import Checker
from include.modules.components import ComponentBaseClass
from include.modules.components.route import Route
import include.modules.utils as utils

class Datastore(ComponentBaseClass):
    
    def __init__(self, cwd, pathbase, *args):
        self.cwd = cwd
        self.pathbase = pathbase

    def copy_templates(self):
        SRC_PREBUILD = os.path.join(self.pathbase, 'prebuild', 'storage', 'datastore')
        SRC_TEMPLATES = os.path.join(self.pathbase, 'templates')
       
        SOURCE_INIT_FILE = os.path.join(self.pathbase, 'prebuild', 'storage', '__init__.py')
        DST_INIT_FILE = os.path.join(self.cwd, 'storage', '__init__.py')        
        
        SOURCE_DS_HANDLER_TEMPLATE = os.path.join(self.pathbase, 'prebuild', 'app', 'handlers', 'datastore_flow_handler.py')
        DST_HANDLER_TEMPLATE = os.path.join(self.cwd, 'app', 'handlers')

        print('Copiando templates de datastore iniciales...')
        DST_PREBUILD = os.path.join(self.cwd, 'storage', 'datastore')

        try:
            shutil.copy(SOURCE_DS_HANDLER_TEMPLATE, DST_HANDLER_TEMPLATE)
        except:
            print("No se puede copiar manejador de datastore que ya existe")
            
        try:
            shutil.copytree(SRC_PREBUILD, DST_PREBUILD)            
        except Exception as e:
            print("No se puede copiar arbol de archivos archivo que ya existe")

        try:
            shutil.copy(SOURCE_INIT_FILE, DST_INIT_FILE)
        except:
            print('Ya existe archivo __init__.py')

    def create_route(self):
        routes_file = os.path.join(self.cwd, 'routes', 'routes.py')
        route_name = 'datastore_flow/entity/<entity>'
        module_handler = 'datastore_flow_handler'
        class_handler = 'DatastoreFlowHandler'
        handler_method = 'datastore_flow_handler'
        utils.create_route(routes_file
                            ,route_name
                            ,module_handler
                            ,class_handler
                            ,handler_method)

    def create(self):
        self.create_route()
        self.copy_templates()