from importlib import import_module

from include.exceptions import exceptions as ex
from include.modules.components.base_component import ComponentBaseClass

def prepare(component_name, *args, **kwargs):
    try:
        if '.' in component_name:
            module_name, class_name = component_name.rsplit('.', 1)
        else:
            module_name = component_name
            class_name = component_name.capitalize()

        component_module = import_module('include.modules.components.' + module_name, package='components')

        component_class = getattr(component_module, class_name)

        instance = component_class(*args, **kwargs)

    except (AttributeError, AssertionError, ImportError):
        raise ex.ComponenteCreationError('Componente: "{}", equivocado'.format(component_name))
    else:
        if not issubclass(component_class, ComponentBaseClass):
            ex.ComponenteCreationError('Componente: "{}", equivocado'.format(component_name))

    return instance