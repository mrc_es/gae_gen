from abc import ABCMeta, abstractmethod


class ComponentBaseClass:
    
    __metaclass__ = ABCMeta

    def __init__(self, cwd, pathbase, setup_info):
        self.cwd = cwd
        self.pathbase = pathbase
        self.setup_info = setup_info
        
    @abstractmethod
    def create(self):
        pass