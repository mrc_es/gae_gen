import ast
import astunparse
import inspect
import os
import shutil

from include.exceptions import exceptions
from include.checkers import accepted_strings as a_s
from include.checkers.checkers import Checker
from include.modules.components import ComponentBaseClass
import include.modules.utils as utils

class Route(ComponentBaseClass):

    def __init__(self, cwd, pathbase, setup_info):
        ComponentBaseClass.__init__(self, cwd, pathbase, setup_info)

    def create_route_handler_info(self):
        if type(self.setup_info) is not dict:
            route = self.setup_info.name
        else:
            route = self.setup_info['name']

        module = route + '_handler'
        class_handler = ''.join(map(lambda x: x[0].upper() + x[1:], route.split("_"))) + "Handler"
        handler_method = module    
        return {
            'route': route,
            'module_handler': module,
            'class_handler': class_handler,
            'handler_method': handler_method }
        
    def create_handler_files(self, new_routes):
        SRC_TEMPLATES = os.path.join(self.pathbase, 'templates')

        for file_name in os.listdir(SRC_TEMPLATES):
            full_file_name = os.path.join(SRC_TEMPLATES, file_name)
            if os.path.isfile(full_file_name) and file_name in a_s.TEMPLATES:
                dest_path = os.path.join(self.cwd, 'app', 'handlers', new_routes['module_handler'] + ".py")
                #shutil.copy(full_file_name, dest_path)
                with open(full_file_name, 'r') as source, open(dest_path, 'w') as dest:
                    contenido = source.read()
                    dest.write(contenido)

        with open(dest_path, 'r') as handler_module:
            new_handler_module = handler_module.read().format(**new_routes)

        with open(dest_path, 'w') as handler_module:
            handler_module.write(new_handler_module)

    def edit_routes_file(self):
        
        route_handler_info = self.create_route_handler_info()
        routes_file = os.path.join(self.cwd, 'routes', 'routes.py')
        utils.create_route(routes_file,
                            route_handler_info['route'],
                            route_handler_info['module_handler'],
                            route_handler_info['class_handler'],
                            route_handler_info['handler_method'])
        return route_handler_info

    def create(self):
        new_routes = self.edit_routes_file()
        self.create_handler_files(new_routes)