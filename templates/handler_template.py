# -*- coding: utf-8 -*-
from functools import partial

from app.exceptions import exceptions
from app.utils import decorators
from app.handlers.flex import FlexibleHandler

"""
Puedes usar algun decorador para cargar un template 
o para generar un json response.
Recuerda ordear tus importes de la forma:
built_in_modules

third-party_modules

local_moules

en orden alfabetico
"""

class {class_handler}(FlexibleHandler):
    
    create_template = partial(decorators.create_template,
                                template_name='default_page')
    @create_template
    def {handler_method}(self, *args, **kwargs):

        if self.request.method == 'GET':
            hello_world_message = "Hello, I'm the handler method: {handler_method}"
            self.template = {{
                'message': hello_world_message
            }}
