# Generador de proyectos de Google App Engine

La finalidad de este framework es desarrollar y probar servicios de App Engine de forma rápida, bien estructurada y automatizada, con conexión al almacenamiento en GCP, además de tener archivos que fomenten la documentación de los proyectos y su control de versiones.

El framework tiene como una finalidad crear una estructura de archivos de la siguiente manera:

```yaml
project:
  app:
    exceptions:
      - __init__.py
      - exceptions.py
      - handlers.py
    handlers:
      - __init__.py
      - flex.py
      - welcome_page.py
      - main_page.py
      ...
    utils:
      - __init__.py
      - utils.py  
      - decorators.py
      - checkers.py
  routes:
    - __init__.py
    - routes.py
  storage:
    datastore:
      - __init__.py
      - ds_entities.py
    cloud_sql:
      - __init__.py
      - queries.py
  resources:
    js:
  	  - js1.js
    templates:
      - template1.html
    styles:
      - style1.css
  random_shit:
    - __init__.py
    - shit_module.py
  lib:
    - help_libraries

  - .gitignore
  - .gg_config.yaml
  - requirements.txt
  - app.yaml
  - appengine_config.py
  - README.md
```

## Características implementadas:

```shell
gg comando [componente] [nombre]

Comandos: 

* create, c         #  Crea algún componente, ya sea el proyecto o alguna ruta con su handler.
* serve, s          #  Ejecuta dev_appserver después de eliminar todos los 
                    #+ archivos .pyc del árbol del proyecto.
* burn, b           #  Elimina toda el árbol del proyecto si su archivo de configuración
                    #+ lo permite.
* deploy, dp        #  Toma elementos del archivo de configuracion y con eso construye la 
                    #+ cadena de despliegue de servicios en GAE

Componentes:

* project, p
* route, r
* s, storage  #Solo disponible para datastore.

Servicios de almacenamiento:
* datastore, ds

Ejemplos:

gg create project proyecto_prueba   #  Crea proyecto 'proyecto_prueba'.
gg c p proyecto_prueba              #  Crea proyecto 'proyecto_prueba' (forma abreviada).
gg create route servicio_uno        #  Crea ruta y handler del servicio 'servicio_uno'.
gg c r servicio_uno                 #  Crea ruta y handler (forma abreviada).
gg c s ds servicio_uno              #  Crea handler y ruta para servicio de almacenamiento
                                    #+ en datastore.
gg serve                            #  Activa dev_appserver.py
gg s                                #  Activa dev_appserver.py (forma abreviada).
gg burn                             #  Elimina árbol el proyecto.
gg b                                #  Elimina el árbol del proyecto (forma abreviada).
gg deploy                           #  Despliega el servicio.
gg dp                               #  Despliega el servicio (forma abreviada).

```

## Notas antes de usar.

Como no hay un instalador, para utilizar el comando `gg.py` se necesita añadir al $PATH la carpeta que contiene al programa `gg.py`.

En caso de que todo esto de flojera, se puede utilizar con su ruta absoluta o relativa.

Para no utilizar la extención ".py", en Windows se puede editar la variable de entorno PATHTEXT añadiendo la extensión ".py", en minúsculas, a la lista de extensiones.
En el caso de una distribucion GNU/Linux se puede hacer un wrapper llamado 'gg' que ejecute `gg.py` con todos los parámetros.

## Servicio de almacenamiento.

Actualmente sólo se tiene un majenador de almacenamiento de datastore.

### Datastore.

Hay varias formas para consultar registros en datastore. Uno de ellos es la búsqueda común. 
Esta búsqueda es la típica en la que se define un modelo de ndb en la ruta del proyecto "/storage/datastore/models.py" y en el handler definido en esa misma carpeta en el archivo "entities_handler.py" se puede hacer uso de ella o se puede importar en un controlador propio usando 

```python 
from storage.datastore import models
```

y operando de la manera convencional.
Sin embargo, parte de los generado por `gg c s ds` es la funcionalidad de poder insertar o 
consultar una entidad sin necesidad de definirla, es decir, con la sola ejecución de ese comando y el despliegue en la nube de lo generado, ya existe esa funcionalidad sin necesidad de programarla.

#### Petición.
Hay varios tipos de consulta, uno es el común que está definido dentro del módulo storage.datastore.entities_handler en la clase CommonSearch.
Ahí se puede especificarlo que se quiere buscar con una consulta dentro de la funcion "__call__". Lo que hará esa función es crear un modelo de ndb con propiedades de clase "GenericProperty" para consultar y sobre ella se aplicarán los filtros que se definan en la función CommonSearch.

Otro tipo de consulta es la llamada **very_lazy**. Esta consulta requiere ciertos parámetros para funcionar.
La consulta completa sigue la siguiente estructura.

```
/datastore_flow/entity/<Entidad>?search_type=<search_type>&part=<part>&values=<values>&rel_op=<operador_relacional>&log_op=<operador_logico>
```

**Entidad**.

  Es el nombre de la entidad sobre la que se quiere buscar.

**search_type**.

  Es alguno de los tipos de búsqueda definidos en el módulo entities_handler.

**part**.

  Es la proyeción de la consulta separada por comas.

**values**.

  Son las claves y valores a comparar separadas por comas. Es decir, si se quiere haer una búsqueda filtrando por un operador relacional, los valores deberán quedar de esta manera: 

  `valor1,valor1_a_comparar,valor2,valor2_a_comparar`

que de cierta manera se traduciría a un
```sql
WHERE 
  valor1 <operador_relacional> valor1_a_comparar
  <operador_logico>
  valor2 <operador_relacional> valor2_a_comparar
```
**operador_relacional**.

  Es una cadena que indica un operador relacional de la siguiente lista: eq, lt, le, gt, ge

**operador_logico**.

  Es una cadena que indica un operador lógico, ya sea AND o OR.

Un ejemplo combinando values, y los operadores sería al añadir en la query string los valores:

`...&values=valor1,valor1_a_comparar,valor2,valor2_a_comparar&rel_op=eq&log_op=AND`

Lo cual se podría ver de la forma
```sql
WHERE 
  valor1 = valor1_a_comparar
  AND
  valor2 = valor2_a_comparar
```
El operador lógico y el operador relacional se repiten en toda la lista de valores por las que se va a filtrar.

Un ejemplo de consulta completa sería de la forma

```
/datastore_flow/entity/Entidad1?search_type=very_lazy&part=campo1,campo2&values=valor1,valor1_a_comparar,valor2,valor2_a_comparar&rel_op=eq&log_op=AND
```

#### Inserción.

La al igual que con la consulta, la entidad no necesariamente tiene que estar definida. Esta se puede crear al vuelo con propiedades genéricas. 

La petición es a través del método POST y sigue la siguiente sintaxis.

```bash
curl --silent \
  --request POST \
  --url "<servicio>/datastore_flow/entity/Entidad1" \
  --data valor1=valor1_a_insertar \
  ...\
  --data valorn=valorn_a_insertar \
```
### Nota de seguridad.

Por la naturaleza de este tipo de métodos es necesario que se pidan reglas de validación sobre campos, entidades y filtros antes de poner en producción estas características.

## Inicio rápido.

Después de instalar el framework y configurar el sistema para que lo pueda usar, para crear un servicio sencillo con almacenamiento en datastore y desplegarlo se siguen los siguientes pasos.
Dentro de una carpeta vacía se ejecutan los comandos.

```bash
$ gg c p proyecto_prueba #  Se crea una carpeta llamada proyecto_prueba con el contenido del
                         #+ proyecto
$ cd proyecto_prueba
$ gg c r ruta1_prueba    #  Creando una ruta llamada ruta1_prueba y su respectivo handler.
$ gg c s ds              #  Creando los archivos de configuración e implementaciones de 
                         #+ búsquedas en datastore.
$ gg serve               #  Para correr el servidor de pruebas.
$ gg dp                  #  Para desplegar el servicio en el proyecto configurado en el 
                         #+ archivo gg_config.yaml.
$ gg burn                #  Para eliminar el arbol del proyecto.

```

### TODO

* Template de error 503.
* Mejora del decorador para templates y responses.
* Conexión con servicios de almacenamiento.
* Continuación de comandos de delete, etc., del framework.
* Despliegue de servicos en GCP con su respectivo archivo de configuracion de despliegues.
* Instalador para poder hacer operaciones de tipo "gg command ..." sin necesidad de tener la ruta del archivo.
* Validación de nombres de proyecto, componentes, etc.
* Agregar archivos de configuración del framework.
* Filtros de validación sobre campos, entidades y filtros de datastore.