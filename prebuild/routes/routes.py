
import webapp2
from app.handlers import *
from app.exceptions import handlers

class Router:
    routes = [
    webapp2.Route('/', handler=main_page.MainPageHandler, handler_method='main_page_handler', methods=['GET']), 
    webapp2.Route('/welcome<:/?>', handler=welcome_page.WelcomePageHandler, handler_method='welcome_page_handler', methods=['GET'])]

    @classmethod
    def get_app(cls):
        r = webapp2.WSGIApplication(cls.routes, debug=True)
        #r.error_handlers[404] = handlers.DefaultHandlers.handle_404
        #r.error_handlers[405] = handlers.DefaultHandlers.handle_405
        #r.error_handlers[500] = handlers.DefaultHandlers.handle_500
        return r

app = Router.get_app()
