# -*- coding: utf-8 -*-
from functools import partial
from app.exceptions import exceptions
from app.utils import decorators
from app.handlers.flex import FlexibleHandler

class MainPageHandler(FlexibleHandler):

    create_template = partial(decorators.create_template,
                                template_name='redirect')
    @create_template
    def main_page_handler(self, *args, **kwargs):

        if self.request.method == 'GET':
        	#self.response.headers['refresh'] = '5;url=/welcome/'
        	pass
