# -*- coding: utf-8 -*-
import os
from functools import partial

from google.appengine.api import app_identity
from google.appengine.api import modules

from app.exceptions import exceptions
from app.utils import decorators
from app.handlers.flex import FlexibleHandler

class WelcomePageHandler(FlexibleHandler):

    create_template = partial(decorators.create_template,
                                template_name='welcome')
    @create_template
    def welcome_page_handler(self, *args, **kwargs):

        if self.request.method == 'GET':
            self.template = {
                "title": app_identity.get_default_version_hostname(),
                "version": modules.get_current_version_name()
            }