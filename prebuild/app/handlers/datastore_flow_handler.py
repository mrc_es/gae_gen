# -*- coding: utf-8 -*-
from functools import partial
import json

from app.exceptions import exceptions
from app.utils import decorators
from app.handlers.flex import FlexibleHandler
from storage.datastore.entities_handler import EntitiesHandler

class DatastoreFlowHandler(FlexibleHandler):
    
    def datastore_flow_handler(self, entity):

        request_elements = self.request.params

        handler = EntitiesHandler(request_elements, self.request.method)

        if self.request.method == 'GET':
            handler.load_search_info(entity_name=entity)
            
            records = handler.get()

            self.response.write(records)
        elif self.request.method == 'POST':
            handler.load_insert_info(entity_name=entity)

            id_entity = handler.post()

            self.response.write(id_entity)

