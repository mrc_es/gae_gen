# -*- coding: utf-8 -*-
import webapp2
import logging
import textwrap
import inspect

from app.utils import utils

class FlexibleHandler(webapp2.RequestHandler):
    
    def format_log_error(self, log):
        trace = '\n'.join("{}: {}".format(*k) for k in enumerate(zip(*inspect.stack()[1:6])[3]))
        exception_log = '''
#-------------------------------------#
#              Error                  #
---------------------------------------
{error}
Stack trace:
{trace}
#-------------------------------------#'''.format(error=textwrap.fill(log, 35), trace=trace)
        logging.error(exception_log)

    def pdumps(self, d): 
        return utils.pretty_dumps(d)
    
    def dumps(self, d): 
        return utils.dumps(d)

    def logi(self, l):
        logging.info(l)

    def loge(self, l):
        logging.error(l)