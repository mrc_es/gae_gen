
class NotFoundError(Exception):
    message= 'Oie no'
    status_code = 404
class MethodNotAllowedError(Exception):
    message = 'Cuidado, lince. Te equivocaste de metodo'
    status_code = 405
class InternalServerError(Exception):
    message= 'GG, saludos'
    status_code = 500
class DataStoreInsertError(Exception):
	message='Error al insertar en datastore'
	status_code = 500
class BadParametersError(Exception):
	status_code = 400
class MissingParametersError(Exception):
	status_code = 400
class NeedIndexError(Exception):
    pass