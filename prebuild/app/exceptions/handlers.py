# -*- coding: utf-8 -*-
import os
import logging
import inspect

from app.handlers.flex import FlexibleHandler

class DefaultHandlers(FlexibleHandler):
    
    resources = '../../resources/templates'

    @classmethod
    def handle_404(cls, request, response, exception):
        path = os.path.join(os.path.split(__file__)[0],  cls.resources, 'NotFoundError.html')
        with open(path, "r") as v:
            response.write(v.read())
        response.set_status(404)

    @classmethod
    def handle_405(cls, request, response, exception):
        path = os.path.join(os.path.split(__file__)[0],  cls.resources, 'MethodNotAllowedError.html')
        with open(path, "r") as v:
            response.write(v.read())
        response.set_status(405)

    @classmethod
    def handle_500(cls, request, response, exception):
        cls().format_log_error(exception.message)
        path = os.path.join(os.path.split(__file__)[0],  cls.resources, 'InternalServerError.html')
        with open(path, "r") as v:
            response.write(v.read())
        response.set_status(500)


class CustomHandlers(FlexibleHandler):
    pass