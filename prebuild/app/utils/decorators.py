# -*- coding: utf-8 -*-
"""decorators.py

Decoradores a utilizar 
"""
import json
import logging
import inspect
import os

from app.exceptions import exceptions

def create_template(funcion, *a, **kw):
    def wrapper(self, *args, **kwargs):
        status_code = 200
        resources = '../../resources/templates'

        try:
            path = os.path.join(os.path.split(__file__)[0],
                                resources,
                                '{}.html'.format(kw['template_name']))
            
            with open(path, "r") as v:  
                template = v.read()
            
            response = funcion(self, *args, **kwargs)
            if hasattr(self, 'template'):
                template = template.format(**self.template)

        except Exception as e:
            
            error = e.__class__.__name__
            
            error_list = [m[0] for m in inspect.getmembers(exceptions, inspect.isclass) if m[1].__module__ == 'app.exceptions.exceptions']

            if error not in error_list:
                self.format_log_error(e.message)
                error = "InternalServerError"

            path = os.path.join(os.path.split(__file__)[0],
                                resources,
                                '{}.html'.format(error))

            with open(path, "r") as v:
                template = v.read()

            if error =='NotFoundError':
                status_code = 404
            elif error =='MethodNotAllowedError':
                status_code = 405
            elif error == 'InternalServerError':
                status_code = 500
        finally:
            self.response.set_status(status_code)
            self.response.write(template)
    return wrapper

def create_response(funcion, *a, **kw):
    
    def wrapper(self, *args, **kwargs):

        response = {
            "success": True,
            "code": 200
        }

        try:
            response['result'] = funcion(self, *args, **kwargs)
        except Exception as e:

            error = e.__class__.__name__
            error_list = [m[0] for m in inspect.getmembers(exceptions, inspect.isclass) if m[1].__module__ == 'app.exceptions.exceptions']
           
            if error not in error_list:
                logging.error(e)
                e = exceptions.InternalServerError

            response["message"] = e.message
            response["code"] = e.status_code
            response["success"] = False
        finally:
            self.response.set_status(response['code'])
            self.response.write(json.dumps(response))
    
    return wrapper