# -*- coding: utf-8 -*-
import json

def pretty_dumps(d):
    return json.dumps(d, indent=3)

def dumps(d):
    return json.dumps(d)