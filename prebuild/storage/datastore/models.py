
from google.appengine.ext import ndb

class EntityTemplate(ndb.Model):
	attr1 = ndb.StringProperty()
	# attr2 = ndb.FloatProperty()
	attr3 = ndb.BooleanProperty()
	attr4 = ndb.IntegerProperty()
	# attr5 = ndb.TextProperty()
	# attr6 = ndb.BlobProperty()
	# attr7 = ndb.DateTimeProperty()
	# attr8 = ndb.DateProperty()
	# attr9 = ndb.TimeProperty()
	# attr10 = ndb.GeoPtProperty()
	# attr11 = ndb.KeyProperty()
	# attr13 = ndb.BlobKeyProperty()
	# attr14 = ndb.UserProperty()
	# attr15 = ndb.StructuredProperty()
	# attr16 = ndb.LocalStructuredProperty()
	# attr17 = ndb.JsonProperty()
	# attr19 = ndb.PickleProperty()
	# attr21 = ndb.GenericProperty()
	# attr23 = ndb.ComputedProperty()
