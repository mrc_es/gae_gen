from abc import ABCMeta, abstractmethod
import inspect
import sys
import logging

from google.appengine.ext import ndb

import storage.datastore.models as models
from app.exceptions import exceptions as ex

class BaseSearchHandler:
   
    #__metaclass__ = ABCMeta

    relational_operators = {
        'eq':'=',
        'lt':'<',
        'le':'<=',
        'gt':'>',
        'ge':'>='
    }

    def __init__(self):
        pass


class SearchStrategy:

    def __init__(self, strategies=None):
        self._strategies = list()
        if strategies is not None:
            self._strategies += strategies

    def search(self, entity, log_op, rel_op, part, values):
        for strategy in self._strategies:
            search = strategy(entity, log_op, rel_op, part, values)
            if search:
                break
        return search        

class VeryLazySearch(BaseSearchHandler):

    '''Busqueda very lazy
    
        Por si se requiere una busqueda rapida y sencilla sin
    necesidad de crear un modelo de datastore

    '''

    def search_projection_strategy(self, entity, log_op, rel_op, part, values):
        aux_list = []

        for i, v in enumerate(values):
            aux_list.append(v)
            if i % 2 == 0:
                aux_list.append(rel_op)

        aux_list_three_values = [ aux_list[i:i+3] for i in range(0,len(aux_list),3) ]

        filter_nodes = [ ndb.FilterNode(*x) for x in aux_list_three_values]

        query = entity.query(projection=part, filters=getattr(ndb, log_op)(*filter_nodes))

        #Revisar tambien:
        #query = ndb.Query(kind=entidad, projection=part, filters=ndb.AND(*filter_nodes))
        try:
            return query.fetch()
        except Exception as e:
            logging.error(e)
            return []
    
    def search_specified_params_strategy(self, entity, log_op, rel_op, part, values):
        aux_list = []

        for i, v in enumerate(values):
            aux_list.append(v)
            if i % 2 == 0:
                aux_list.append(rel_op)

        aux_list_three_values = [ aux_list[i:i+3] for i in range(0,len(aux_list),3) ]

        filter_nodes = [ ndb.FilterNode(*x) for x in aux_list_three_values]
        query = entity.query(filters=getattr(ndb, log_op)(*filter_nodes))
        logging.info(query)
        logging.info(query.fetch())
        try:
            return query.fetch()
        except:
            return []

    def __call__(self, entity_name, request_elements, entity_factory=None):

        part = request_elements.get('part').split(',')
        values = request_elements.get('values').split(',')

        log_op = str(request_elements.get('log_op')).upper()
        rel_op = self.relational_operators[request_elements.get('rel_op')]

        entity = entity_factory(entity_name, part + values[::2])

        search = SearchStrategy([
                self.search_projection_strategy,
                self.search_specified_params_strategy
            ])

        return search.search(entity, log_op, rel_op, part, values)

class CommonSearch:
    '''Busqueda comun
    
        En caso de usar la forma tradicional y requerir un modelo
    de Datastore definnido en un archivo.
    '''
    def __call__(self, entity_name, request_elements, entity_factory=None):

        try:
            entity = getattr(models, entity_name)
        except AttributeError:
            entity = entity_factory(entity_name, request_elements.keys())
        q = entity.query(entity.attr1 == request_elements.get('attr1'))

        return q.fetch()

class InsertToDS:

    def insert_to_ds(self, entity, parent_key=None, id_entity=None, **kwargs):

        entidad = entity(parent=parent_key, id=id_entity, **kwargs)
        return entidad.put()

    def __call__(self, entity_name, request_elements, entity_factory=None):

        try:
            entity = getattr(models, entity_name)
        except AttributeError:
            entity = entity_factory(entity_name, request_elements.keys())

        id_entity = self.insert_to_ds(entity,**request_elements)

        return id_entity

class EntitiesHandler:

    def __init__(self, dct, method):
        self.request_elements = dct
        self.method = method
    
    def validate_info(self,**kwargs):
        
        self.missing_parameters = ','.join(
            map(
                 '"{0}"'.format
                ,list( set(self.required_fields[self.method]) - set(kwargs) )
            )
        )

        if self.missing_parameters:
            raise ex.MissingParametersError("Parametros faltantes")


    def entity_factory(self, entity_name, entity_attributes):

        ndb_attr = {}

        for element in entity_attributes:
            ndb_attr[element] = ndb.GenericProperty(indexed=True)

        return type(entity_name, (ndb.Model,), ndb_attr)

    def load_search_info(self, entity_name=None):
        clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
        
        if entity_name:
            
            self.entity_name = entity_name
            search_type = ''
            search_type = self.request_elements.get('search_type')

            if not search_type:
                search_type = 'CommonSearch'
            else:
                search_type = ''.join(
                    map(lambda x: x.capitalize(), search_type.split("_"))
                ) + "Search"
            
            self.search_type = filter(lambda x: x[0] == search_type, clsmembers)[0][1]

    def get(self):
        result = self.search_type()
        return result(self.entity_name,self.request_elements, entity_factory=self.entity_factory)

    def load_insert_info(self, entity_name=None):
        self.entity_name = entity_name

    def post(self):

        entidad = InsertToDS()
        
        return entidad(self.entity_name, self.request_elements, self.entity_factory)
